package com.task64s10.restapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.hibernate.internal.util.beans.BeanInfoHelper.ReturningBeanInfoDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task64s10.restapi.model.CVoucher;
import com.task64s10.restapi.respository.IVoucherRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CVoucherController {

    @Autowired
    IVoucherRepository pVoucherRepository;

    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getAllVouchers() {
        try {
            List<CVoucher> pVouchers = new ArrayList<CVoucher>();
            // Lấy ra danh sách vouchers từ DB
            return new ResponseEntity<>(pVouchers, HttpStatus.OK);

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    @GetMapping("/vouchers/{id}")
    public ResponseEntity<CVoucher> getCVoucherById(@PathVariable("id") long id) {
        // lấy voucher theo id
        Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
        if (voucherData.isPresent()) {
            return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);

        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/vouchers")
    public ResponseEntity<Object> createCVoucher(@Valid @RequestBody CVoucher pCVoucher) {
        try {
            Optional<CVoucher> voucherData = pVoucherRepository.findById(pCVoucher.getId());
            if (voucherData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Voucher already exsit  ");

            }
            pCVoucher.setNgayTao(pCVoucher.getNgayTao());
            pCVoucher.setNgayCapNhat(pCVoucher.getNgayCapNhat());
            CVoucher _vouchers = pVoucherRepository.save(pCVoucher);
            return new ResponseEntity<>(_vouchers, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            // Hiện thông báo lỗi tra back-end
            // return new ResponseEntity<>(e.getCause().getCause().getMessage(),
            // HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping("/vouchers/{id}")
    public ResponseEntity<Object> updateCvoucherId(@PathVariable("id") long id, @RequestBody CVoucher pVouchers) {
        Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
        if (voucherData.isPresent()) {
            CVoucher voucher = voucherData.get();
            voucher.setMaVoucher(pVouchers.getMaVoucher());
            voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
            voucher.setGhiChu(pVouchers.getGhiChu());
            voucher.setNgayCapNhat(pVouchers.getNgayCapNhat());
            voucher.setNgayTao(pVouchers.getNgayTao());
            try {
                return new ResponseEntity<>(pVoucherRepository.save(voucher), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Voucher:" + e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified Voucher: " + id + "  for update.");
        }
    }

    @DeleteMapping("/vouchers/{id}")
    public ResponseEntity<CVoucher> deleteCVoucherById(@PathVariable("id") long id) {
        try {
            pVoucherRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @DeleteMapping("/vouchers")
    public ResponseEntity<CVoucher> deleteAllVoucher() {
        try {
            pVoucherRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

}
