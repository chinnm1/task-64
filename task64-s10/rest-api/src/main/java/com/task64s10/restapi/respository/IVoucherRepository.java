package com.task64s10.restapi.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task64s10.restapi.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {

}
