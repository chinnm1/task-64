package com.task64s10.restapi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.hibernate.internal.util.beans.BeanInfoHelper.ReturningBeanInfoDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task64s10.restapi.model.CDrink;
import com.task64s10.restapi.respository.IDrinkRepository;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CDrinkController {

    @Autowired
    IDrinkRepository pDrinkRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAlldrinks() {
        try {
            List<CDrink> pDrink = new ArrayList<CDrink>();
            // Lấy ra danh sách drinks từ DB
            pDrinkRepository.findAll().forEach(pDrink::add);

            return new ResponseEntity<>(pDrink, HttpStatus.OK);

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    @GetMapping("/drinks/{id}")
    public ResponseEntity<CDrink> getDrinkById(@PathVariable("id") long id) {
        // lấy drink theo id
        Optional<CDrink> drinkData = pDrinkRepository.findById(id);
        if (drinkData.isPresent()) {
            return new ResponseEntity<>(drinkData.get(), HttpStatus.OK);

        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/drinks")
    public ResponseEntity<Object> createCdrink(@Valid @RequestBody CDrink pDrink) {
        try {
            Optional<CDrink> drinkData = pDrinkRepository.findById(pDrink.getId());
            if (drinkData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Drink already exsit ");

            }
            pDrink.setNgayTao(new Date());
            pDrink.setNgayCapNhat(null);
            CDrink _drink = pDrinkRepository.save(pDrink);
            return new ResponseEntity<>(_drink, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            // Hiện thông báo lỗi tra back-end
            // return new ResponseEntity<>(e.getCause().getCause().getMessage(),
            // HttpStatus.INTERNAL_SERVER_ERROR);
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Drink" + e.getCause().getCause().getMessage());
        }

    }

    @PutMapping("/drinks/{id}")
    public ResponseEntity<Object> updateCdrinkId(@PathVariable("id") long id,
            @RequestBody CDrink pDrink) {
        Optional<CDrink> drinkData = pDrinkRepository.findById(id);
        if (drinkData.isPresent()) {
            CDrink drink = drinkData.get();
            drink.setMaNuocUong(pDrink.getMaNuocUong());
            drink.setTenNuocUong(pDrink.getTenNuocUong());
            drink.setDonGia(pDrink.getDonGia());
            drink.setGhiChu(pDrink.getGhiChu());
            drink.setNgayCapNhat(new Date());

            try {
                return new ResponseEntity<>(pDrinkRepository.save(drink), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified drink:" +
                                e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified drink: " +
                    id + " for update.");
        }
    }

    @DeleteMapping("/drinks/{id}")
    public ResponseEntity<CDrink> deleteCdrinkById(@PathVariable("id") long id) {
        try {
            pDrinkRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @DeleteMapping("/drinks")
    public ResponseEntity<CDrink> deleteAlldrink() {
        try {
            pDrinkRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

}
